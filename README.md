# Trip Love

Trip Love is a mini-app project for the Trip training program. The purpose of this project is to provide a platform for all the single members of Trip company to find true love.



## 项目结构
* Backend 后端工程文件
* Frontend 前端工程文件
* Document 存放产品，设计，PPT等文件
* (可扩充)

## 协作平台
[miro协作](https://miro.com/welcomeonboard/XnukyfA1nMQXQz3e82upQs9zxdyie0EN8XLiRs8l6DqCTILcnF517aLbX6hhDSgk)

[PPT设计](https://docs.google.com/presentation/d/1lv-c2YYicYi8iWgThpwsv8OGsvSqGLoPvHxNA9GvIkE/edit?usp=sharing)

## 前端
[github 小程序 实现探探滑动效果功能](https://github.com/wqs565/slider)


## 后端


## 算法
推荐系统不需要实现


## 测试




