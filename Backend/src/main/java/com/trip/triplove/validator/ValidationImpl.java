package com.trip.triplove.validator;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Set;

@Component //表明它是一个Spring bean，在进行类扫描的时候可以被扫描到
public class ValidationImpl implements InitializingBean {

    //选择javax包的,是真正通过javax定义的一个接口实现validator的工具，靠它来检验字段
    private Validator validator;

    //实现校验方法并返回校验结果
    public ValidationResult validate(Object bean){
        ValidationResult result = new ValidationResult();
        //真正校验bean，参数为bean，返回一个包含错误信息的Set
        Set<ConstraintViolation<Object>> constraintViolationSet= validator.validate(bean);
        //有异常
        if(constraintViolationSet.size() > 0){
            result.setHasError(true);
            //lambada表达式遍历constraintViolationSet
            constraintViolationSet.forEach(constraintViolation ->{
                String errMsg = constraintViolation.getMessage();
                //获取出错字段
                String propertyName = constraintViolation.getPropertyPath().toString();
                result.getErrMsgMap().put(propertyName, errMsg);
            });
        }
        return result;
    }
    @Override
    //当Spring bean初始化完成之后，会回调这个方法
    public void afterPropertiesSet() throws Exception {
        //将hibernate validator通过工厂的初始化方式使其实例化
        validator = Validation.buildDefaultValidatorFactory().getValidator();
    }
}
