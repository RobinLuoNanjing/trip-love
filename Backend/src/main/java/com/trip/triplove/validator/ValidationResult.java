package com.trip.triplove.validator;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

public class ValidationResult {
    //校验结果是否有错
    private boolean hasError = false;

    //存放错误信息的map
    private Map<String, String> errMsgMap = new HashMap<>();

    //实现通过格式化字符串信息获取错误结果的msg方法,因为javabean可能有多个字段出现问题，因为是将错误信息传到前端，所以定义为string
    public String getErrMsg(){
        return StringUtils.join(errMsgMap.values().toArray(),",");
    }

    public boolean isHasError() {
        return hasError;
    }

    public void setHasError(boolean hasError) {
        this.hasError = hasError;
    }

    public Map<String, String> getErrMsgMap() {
        return errMsgMap;
    }

    public void setErrMsgMap(Map<String, String> errMsgMap) {
        this.errMsgMap = errMsgMap;
    }


}
