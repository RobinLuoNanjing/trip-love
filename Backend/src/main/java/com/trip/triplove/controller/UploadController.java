package com.trip.triplove.controller;

import com.trip.triplove.error.BusinessException;
import com.trip.triplove.error.EmBusinessError;
import com.trip.triplove.response.CommonReturnType;
import com.trip.triplove.util.CommonUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.StandardMultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Random;
import java.util.UUID;

import static com.trip.triplove.error.EmBusinessError.FILE_UPLOAD_FAIL;


@Controller
public class UploadController {

    private final static String FILE_UPLOAD_DIC = "D:\\ctrip\\userImage\\upload\\";//上传文件的默认url前缀，根据部署设置自行修改

//    @PostMapping({"/upload/file"})
//    @ResponseBody
//    public CommonReturnType upload(HttpServletRequest httpServletRequest, @RequestParam("file") MultipartFile file) throws URISyntaxException {
//        String fileName = file.getOriginalFilename();
//        //后缀名
//        String suffixName = fileName.substring(fileName.lastIndexOf("."));
//        //生成文件名称通用方法
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
//        Random r = new Random();
//        StringBuilder tempName = new StringBuilder();
//        tempName.append(sdf.format(new Date())).append(r.nextInt(100)).append(suffixName);
//        String newFileName = tempName.toString();
//        File fileDirectory = new File(FILE_UPLOAD_DIC);
//        //创建文件
//        File destFile = new File(FILE_UPLOAD_DIC + newFileName);
//        try {
//            if (!fileDirectory.exists()) {
//                if (!fileDirectory.mkdir()) {
//                    throw new IOException("文件夹创建失败,路径为：" + fileDirectory);
//                }
//            }
//            file.transferTo(destFile);
//            System.out.println(destFile.getName());
//            //Result resultSuccess = ResultGenerator.genSuccessResult();
//            //resultSuccess.setData(NewBeeMallUtils.getHost(new URI(httpServletRequest.getRequestURL() + "")) + "/upload/" + newFileName);
//
//            System.out.println(CommonUtils.getHost(new URI(httpServletRequest.getRequestURL() + "")) + "/upload/" + newFileName);
//            return CommonReturnType.create(CommonUtils.getHost(new URI(httpServletRequest.getRequestURL() + "")) + "/upload/" + newFileName);
//        } catch (IOException e) {
//            e.printStackTrace();
//            //return CommonReturnType.("文件上传失败");
//            throw new BusinessException(FILE_UPLOAD_FAIL);
//        }
//    }


    @PostMapping("upload/picture")
    @ResponseBody
    public String uploadPicture(HttpServletRequest request, HttpServletResponse response) throws Exception {
        System.out.println("enter");
        String filePath = "";
        request.setCharacterEncoding("utf-8"); //设置编码
        String realPath = request.getSession().getServletContext().getRealPath("/uploadFile/");
        File dir = new File(realPath);
        //文件目录不存在，就创建一个
        if (!dir.isDirectory()) {
            dir.mkdirs();
        }
        try {
            StandardMultipartHttpServletRequest req = (StandardMultipartHttpServletRequest) request;
            //获取formdata的值
            Iterator<String> iterator = req.getFileNames();
            String username = request.getParameter("username");
            String password = request.getParameter("password");
            String timedata = request.getParameter("timedata");

            while (iterator.hasNext()) {
                MultipartFile file = req.getFile(iterator.next());
                String fileName = file.getOriginalFilename();
                //真正写到磁盘上
                String uuid = UUID.randomUUID().toString().replace("-", "");
                String kzm = fileName.substring(fileName.lastIndexOf("."));
                String filename = uuid + kzm;
                File file1 = new File(realPath + filename);
                OutputStream out = new FileOutputStream(file1);
                out.write(file.getBytes());
                out.close();
                filePath = request.getScheme() + "://" +
                        request.getServerName() + ":"
                        + request.getServerPort()
                        + "/uploadFile/" + filename;
                System.out.println("访问图片路径:" + filePath + "====username:" + username);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return filePath;
    }


    @PostMapping({"/upload/file"})
    @ResponseBody
    public CommonReturnType upload(HttpServletRequest httpServletRequest) throws URISyntaxException {
        StandardMultipartHttpServletRequest req = (StandardMultipartHttpServletRequest) httpServletRequest;

        //获取formdata的值
        Iterator<String> iterator = req.getFileNames();
        MultipartFile file = req.getFile(iterator.next());
        String fileName = file.getOriginalFilename();
        //后缀名
        String suffixName = fileName.substring(fileName.lastIndexOf("."));
        //生成文件名称通用方法
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
        Random r = new Random();
        StringBuilder tempName = new StringBuilder();
        tempName.append(sdf.format(new Date())).append(r.nextInt(100)).append(suffixName);
        String newFileName = tempName.toString();
        File fileDirectory = new File(FILE_UPLOAD_DIC);
        //创建文件
        File destFile = new File(FILE_UPLOAD_DIC + newFileName);
        try {
            if (!fileDirectory.exists()) {
                if (!fileDirectory.mkdir()) {
                    throw new IOException("文件夹创建失败,路径为：" + fileDirectory);
                }
            }
            file.transferTo(destFile);
            System.out.println(destFile.getName());
            //Result resultSuccess = ResultGenerator.genSuccessResult();
            //resultSuccess.setData(NewBeeMallUtils.getHost(new URI(httpServletRequest.getRequestURL() + "")) + "/upload/" + newFileName);

            System.out.println(CommonUtils.getHost(new URI(httpServletRequest.getRequestURL() + "")) + "/upload/" + newFileName);
            return CommonReturnType.create(CommonUtils.getHost(new URI(httpServletRequest.getRequestURL() + "")) + "/upload/" + newFileName);
        } catch (IOException e) {
            e.printStackTrace();
            //return CommonReturnType.("文件上传失败");
            throw new BusinessException(FILE_UPLOAD_FAIL);
        }
    }
}
