package com.trip.triplove.controller;

import com.trip.triplove.mbg.model.UserInfo;
import com.trip.triplove.response.CommonReturnType;
import com.trip.triplove.service.MatchingService;
import com.trip.triplove.service.WeChatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;

@Controller
public class MatchingController extends BaseController{ 

    @Autowired
    private MatchingService matchingService;

    @Autowired
    private WeChatService weChatService;


    @RequestMapping(value = "/getSoulMate/{id}",method = RequestMethod.GET)
    @ResponseBody
    public CommonReturnType getSoulMate(@PathVariable String id){
        //id = weChatService.codetoopenid(id);
        List<UserInfo> soulMateInfo = matchingService.getUserSoulMate(id);
        if(soulMateInfo==null) return CommonReturnType.create(null);
        else return CommonReturnType.create(soulMateInfo);
    }

    @RequestMapping(value = "/getLikesMe/{id}",method = RequestMethod.GET)
    @ResponseBody
    public CommonReturnType getLikeMe(@PathVariable String id){
        //id = weChatService.codetoopenid(id);
        List<UserInfo> likesMe = matchingService.whoLikesMe(id);
        if(likesMe==null) return CommonReturnType.create(null);
        return CommonReturnType.create(likesMe);
    }

    @RequestMapping(value = "/loveClick",method = RequestMethod.POST,consumes = {CONTENT_TYPE_FORMED})
    @ResponseBody
    public CommonReturnType loveClick(HttpServletRequest request, @RequestParam(name = "mainWorkerId",required = false) String mainWorkerId, @RequestParam(name = "subWorkerId",required = false) String subWorkerId, @RequestParam(name = "isLike",required = false)  boolean isLike){
//        System.out.println(request.getRequestURL());
//        System.out.println(request.);


//        BufferedReader br = null;
//        StringBuilder sb = new StringBuilder("");
//        try
//        {
//            br = request.getReader();
//            String str;
//            while ((str = br.readLine()) != null)
//            {
//                sb.append(str);
//            }
//            br.close();
//        }
//        catch (IOException e)
//        {
//            e.printStackTrace();
//        }
//        finally
//        {
//            if (null != br)
//            {
//                try
//                {
//                    br.close();
//                }
//                catch (IOException e)
//                {
//                    e.printStackTrace();
//                }
//            }
//        }
//
//        System.out.println(sb.toString());
        //mainWorkerId = weChatService.codetoopenid(mainWorkerId);
        return CommonReturnType.create(matchingService.loveClick(mainWorkerId, subWorkerId, isLike));
    }

}
