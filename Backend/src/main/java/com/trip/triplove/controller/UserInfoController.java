package com.trip.triplove.controller;

import com.alibaba.druid.util.StringUtils;
import com.trip.triplove.error.BusinessException;
import com.trip.triplove.error.EmBusinessError;
import com.trip.triplove.mbg.model.UserInfo;
import com.trip.triplove.response.CommonReturnType;
import com.trip.triplove.service.UserInfoService;
import com.trip.triplove.service.WeChatService;
import com.trip.triplove.service.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

import java.util.ArrayList;
import java.util.List;

import static com.trip.triplove.error.EmBusinessError.PARAMETER_VALIDATION_ERROR;

@Controller
public class UserInfoController extends BaseController{

    @Autowired
    private UserInfoService userInfoService;

    @Autowired
    private HttpServletRequest httpServletRequest;

    @Autowired
    private WeChatService weChatService;

    //用户登陆接口
    @RequestMapping(value = "/getUserInfo", method = {RequestMethod.GET})
    @ResponseBody
    public CommonReturnType getUserInfo(@RequestParam(name = "workerId")String workerId) throws BusinessException {
        //workerId = weChatService.codetoopenid(workerId);
        //参数判空
        if(StringUtils.isEmpty(workerId)){
            throw new BusinessException(PARAMETER_VALIDATION_ERROR);
        }

        //workerId不存在会报异常
        UserInfo userInfo = userInfoService.getUserInfo(workerId);

        //将用户凭证假如到用户登陆成功的session中基于cookie
//        httpServletRequest.getSession().setAttribute("IS_LOGIN", true);
//        httpServletRequest.getSession().setAttribute("LOGIN_USER", userInfo);

        return CommonReturnType.create(userInfo);
    }


    @RequestMapping(value = "/register", method = {RequestMethod.POST}, consumes = {CONTENT_TYPE_FORMED})
    @ResponseBody
    public CommonReturnType register(UserInfo userInfo){
        //userInfo.setWorkerId(weChatService.codetoopenid(userInfo.getWorkerId()));
        System.out.println(userInfo);
        userInfoService.register(userInfo);
        return CommonReturnType.create(null);
    }

    @RequestMapping(value = "/editUserInfo", method = {RequestMethod.POST}, consumes = {CONTENT_TYPE_FORMED})
    @ResponseBody
    public CommonReturnType editUser(UserInfo userInfo){
        System.out.println(userInfo);
        userInfoService.editUserInfo(userInfo);
        return CommonReturnType.create(null);
    }

    @RequestMapping(value = "/userList", method = {RequestMethod.GET})
    @ResponseBody
    public CommonReturnType userList(@RequestParam(name = "workerId") String workerId,
                                     @RequestParam(name = "minAge", required = false)Integer minAge,
                                     @RequestParam(name = "maxAge", required = false)Integer maxAge,
                                     @RequestParam(name = "provinces", required = false) List<String> provinces,
                                     @RequestParam(name = "pageNum", defaultValue = "1") Integer pageNum,
                                     @RequestParam(name = "pageSize", defaultValue = "3") Integer pageSize,
                                     @RequestParam(name = "sex", required = false) String sex){
        //workerId = weChatService.codetoopenid(workerId);
        System.out.println(workerId);
        UserInfo userInfo ;
        if(minAge == null && maxAge == null && provinces == null || sex == null){
            userInfo = userInfoService.getUserInfo(workerId);
            provinces = new ArrayList<>();
            provinces.add(userInfo.getProvince());
            int age = userInfo.getAge();
            minAge = (age / 10) * 10;
            maxAge = (age / 10 + 1) * 10;
            if(sex == null){
                sex = "男";
                if(userInfo.getSex().trim().equals("男"))
                    sex = "女";
            }
        }

        if(minAge == null) minAge = 18;
        if(maxAge == null) maxAge = 100;
        List<User> userList = userInfoService.getUserList(workerId, minAge, maxAge, provinces, pageNum, pageSize, sex);
        return CommonReturnType.create(userList);
    }

    @RequestMapping(value = "/getOpenId", method = {RequestMethod.GET})
    @ResponseBody
    public CommonReturnType getOpenId(@RequestParam(name = "openId")String openId) throws BusinessException {
        return CommonReturnType.create(weChatService.codetoopenid(openId));
    }

}
