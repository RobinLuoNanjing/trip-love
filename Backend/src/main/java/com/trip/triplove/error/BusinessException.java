package com.trip.triplove.error;

//包装器业务异常类实现
public class BusinessException extends RuntimeException implements CommonError {
    private CommonError commonError;
    public BusinessException(CommonError commonError){
        super();
        this.commonError = commonError;
    }
    //自定义错误信息
    public BusinessException(CommonError commonError, String errMsg){
        super();
        this.commonError = commonError;
        this.commonError.setErrMsg(errMsg);
    }
    @Override
    public int getErrCode() {
        return this.commonError.getErrCode();
    }
    @Override
    public String getErrMsg() {
        return this.commonError.getErrMsg();
    }
    @Override
    public void setErrMsg(String errMsg) {
        this.commonError.setErrMsg(errMsg);
        //return this;
    }
}
