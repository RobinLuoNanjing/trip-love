package com.trip.triplove.error;

public enum EmBusinessError implements CommonError {
    //通用错误类型10000
    PARAMETER_VALIDATION_ERROR(10001,"参数不合法"),
    UNKNOW_ERROR(10002,"未知错误"),

    //20000开头为用户信息相关错误定义
    USER_NOT_EXIST(20001, "用户不存在"),

    //30000开头为图片上传相关错误
    FILE_UPLOAD_FAIL(30001, "文件上传失败");

    private int errCode;
    private String errMsg;

    private EmBusinessError(int errCode, String errMsg){
        this.errCode = errCode;
        this.errMsg = errMsg;
    }

    @Override
    public int getErrCode() {
        return errCode;
    }

    @Override
    public String getErrMsg() {
        return errMsg;
    }

    @Override
    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
        //return this;
    }

    public String toString(){
        return "errCode = " + errCode + ", errMsg = " + errMsg;
    }
}
