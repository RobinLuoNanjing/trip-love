package com.trip.triplove.mbg.mapper;

import com.trip.triplove.mbg.model.UserInfo;
import com.trip.triplove.mbg.model.UserInfoExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UserInfoMapper {
    long countByExample(UserInfoExample example);

    int deleteByExample(UserInfoExample example);

    int deleteByPrimaryKey(String workerId);

    int insert(UserInfo record);

    int insertSelective(UserInfo record);

    List<UserInfo> selectByExample(UserInfoExample example);

    UserInfo selectByPrimaryKey(String workerId);

    int updateByExampleSelective(@Param("record") UserInfo record, @Param("example") UserInfoExample example);

    int updateByExample(@Param("record") UserInfo record, @Param("example") UserInfoExample example);

    int updateByPrimaryKeySelective(UserInfo record);

    int updateByPrimaryKey(UserInfo record);

    List<UserInfo> selectByAgeAndProvinces(@Param("workerId")String workerId, @Param("minAge") Integer minAge, @Param("maxAge") Integer maxAge, @Param("provinces") List<String> provinces, @Param("sex") String sex);

    int updateSoulMateByPrimaryKey(@Param("workerId") String workerId , @Param("soulMateId") String soulMateId);
}