package com.trip.triplove.mbg.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.Date;

public class MatchingRecord implements Serializable {
    private Integer id;

    private String mainWorkerId;

    @NotBlank(message = "未知喜欢的员工编号")
    private String subWorkerId;

    @NotEmpty(message = "喜欢状态未确定")
    private Integer likeState;

    private Date likeDate;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMainWorkerId() {
        return mainWorkerId;
    }

    public void setMainWorkerId(String mainWorkerId) {
        this.mainWorkerId = mainWorkerId;
    }

    public String getSubWorkerId() {
        return subWorkerId;
    }

    public void setSubWorkerId(String subWorkerId) {
        this.subWorkerId = subWorkerId;
    }

    public Integer getLikeState() {
        return likeState;
    }

    public void setLikeState(Integer likeState) {
        this.likeState = likeState;
    }

    public Date getLikeDate() {
        return likeDate;
    }

    public void setLikeDate(Date likeDate) {
        this.likeDate = likeDate;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", mainWorkerId=").append(mainWorkerId);
        sb.append(", subWorkerId=").append(subWorkerId);
        sb.append(", likeState=").append(likeState);
        sb.append(", likeDate=").append(likeDate);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}