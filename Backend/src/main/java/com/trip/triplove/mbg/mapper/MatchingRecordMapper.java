package com.trip.triplove.mbg.mapper;

import com.trip.triplove.mbg.model.MatchingRecord;
import com.trip.triplove.mbg.model.MatchingRecordExample;
import java.util.List;

import com.trip.triplove.mbg.model.UserInfo;
import org.apache.ibatis.annotations.Param;

public interface MatchingRecordMapper {
    long countByExample(MatchingRecordExample example);

    int deleteByExample(MatchingRecordExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(MatchingRecord record);

    int insertSelective(MatchingRecord record);

    List<UserInfo> selectSoulMateByWorkerid(String workerId);

    List<UserInfo> selectLikesmeByWokerid(String workerId);

    List<MatchingRecord> selectByExample(MatchingRecordExample example);

    MatchingRecord selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") MatchingRecord record, @Param("example") MatchingRecordExample example);

    int updateByExample(@Param("record") MatchingRecord record, @Param("example") MatchingRecordExample example);

    int updateByPrimaryKeySelective(MatchingRecord record);

    int updateByPrimaryKey(MatchingRecord record);

    MatchingRecord selectByMainAndSub(@Param("mainWorkerId") String mainWorkerId, @Param("subWorkerId") String subWorkerId);

    int updateLikeState(@Param("mainWorkerId") String mainWorkerId, @Param("subWorkerId") String subWorkerId, @Param("likeState") Integer likeState);
}