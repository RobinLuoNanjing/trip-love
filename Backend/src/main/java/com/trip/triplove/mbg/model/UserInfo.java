package com.trip.triplove.mbg.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel
public class UserInfo implements Serializable {

    @NotBlank(message = "员工编号未确定")
    @ApiModelProperty(name = "工号", required = true, notes = "主体标记")
    private String workerId;

    @NotBlank(message = "员工姓名未填写")
    private String userName;

    //@NotBlank(message = "员工部门未填写")
    private String bu;

    //@NotBlank(message = "员工职位未填写")
    private String position;

    @NotBlank(message = "城市未填写")
    private String city;

    @NotNull(message = "身高未填写")
    private Integer height;

    @NotNull(message = "年龄未填写")
    private Integer age;

    @NotBlank(message = "爱好未填写")
    private String hobbies;

    //@NotBlank(message = "个性签名未填写")
    private String sign;

    @NotBlank(message = "星座未填写")
    private String cc;

    @NotBlank(message = "学校未填写")
    private String school;

    private String soulmate;

    @NotBlank(message = "照片未上传")
    private String photo;

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    @NotBlank(message = "性别未填写")
    private String sex;

    @NotBlank(message = "省份未填写")
    private String province;

    private static final long serialVersionUID = 1L;

    public String getWorkerId() {
        return workerId;
    }

    public void setWorkerId(String workerId) {
        this.workerId = workerId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getBu() {
        return bu;
    }

    public void setBu(String bu) {
        this.bu = bu;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getHobbies() {
        return hobbies;
    }

    public void setHobbies(String hobbies) {
        this.hobbies = hobbies;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getSoulmate() {
        return soulmate;
    }

    public void setSoulmate(String soulmate) {
        this.soulmate = soulmate;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", workerId=").append(workerId);
        sb.append(", userName=").append(userName);
        sb.append(", bu=").append(bu);
        sb.append(", position=").append(position);
        sb.append(", province=").append(province);
        sb.append(", city=").append(city);
        sb.append(", height=").append(height);
        sb.append(", age=").append(age);
        sb.append(", hobbies=").append(hobbies);
        sb.append(", sign=").append(sign);
        sb.append(", cc=").append(cc);
        sb.append(", school=").append(school);
        sb.append(", soulmate=").append(soulmate);
        sb.append(", photo=").append(photo);
        sb.append(", sex=").append(sex);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}