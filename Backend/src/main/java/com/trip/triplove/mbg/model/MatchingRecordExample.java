package com.trip.triplove.mbg.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MatchingRecordExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public MatchingRecordExample() {
        oredCriteria = new ArrayList<>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andMainWorkerIdIsNull() {
            addCriterion("main_worker_id is null");
            return (Criteria) this;
        }

        public Criteria andMainWorkerIdIsNotNull() {
            addCriterion("main_worker_id is not null");
            return (Criteria) this;
        }

        public Criteria andMainWorkerIdEqualTo(String value) {
            addCriterion("main_worker_id =", value, "mainWorkerId");
            return (Criteria) this;
        }

        public Criteria andMainWorkerIdNotEqualTo(String value) {
            addCriterion("main_worker_id <>", value, "mainWorkerId");
            return (Criteria) this;
        }

        public Criteria andMainWorkerIdGreaterThan(String value) {
            addCriterion("main_worker_id >", value, "mainWorkerId");
            return (Criteria) this;
        }

        public Criteria andMainWorkerIdGreaterThanOrEqualTo(String value) {
            addCriterion("main_worker_id >=", value, "mainWorkerId");
            return (Criteria) this;
        }

        public Criteria andMainWorkerIdLessThan(String value) {
            addCriterion("main_worker_id <", value, "mainWorkerId");
            return (Criteria) this;
        }

        public Criteria andMainWorkerIdLessThanOrEqualTo(String value) {
            addCriterion("main_worker_id <=", value, "mainWorkerId");
            return (Criteria) this;
        }

        public Criteria andMainWorkerIdLike(String value) {
            addCriterion("main_worker_id like", value, "mainWorkerId");
            return (Criteria) this;
        }

        public Criteria andMainWorkerIdNotLike(String value) {
            addCriterion("main_worker_id not like", value, "mainWorkerId");
            return (Criteria) this;
        }

        public Criteria andMainWorkerIdIn(List<String> values) {
            addCriterion("main_worker_id in", values, "mainWorkerId");
            return (Criteria) this;
        }

        public Criteria andMainWorkerIdNotIn(List<String> values) {
            addCriterion("main_worker_id not in", values, "mainWorkerId");
            return (Criteria) this;
        }

        public Criteria andMainWorkerIdBetween(String value1, String value2) {
            addCriterion("main_worker_id between", value1, value2, "mainWorkerId");
            return (Criteria) this;
        }

        public Criteria andMainWorkerIdNotBetween(String value1, String value2) {
            addCriterion("main_worker_id not between", value1, value2, "mainWorkerId");
            return (Criteria) this;
        }

        public Criteria andSubWorkerIdIsNull() {
            addCriterion("sub_worker_id is null");
            return (Criteria) this;
        }

        public Criteria andSubWorkerIdIsNotNull() {
            addCriterion("sub_worker_id is not null");
            return (Criteria) this;
        }

        public Criteria andSubWorkerIdEqualTo(String value) {
            addCriterion("sub_worker_id =", value, "subWorkerId");
            return (Criteria) this;
        }

        public Criteria andSubWorkerIdNotEqualTo(String value) {
            addCriterion("sub_worker_id <>", value, "subWorkerId");
            return (Criteria) this;
        }

        public Criteria andSubWorkerIdGreaterThan(String value) {
            addCriterion("sub_worker_id >", value, "subWorkerId");
            return (Criteria) this;
        }

        public Criteria andSubWorkerIdGreaterThanOrEqualTo(String value) {
            addCriterion("sub_worker_id >=", value, "subWorkerId");
            return (Criteria) this;
        }

        public Criteria andSubWorkerIdLessThan(String value) {
            addCriterion("sub_worker_id <", value, "subWorkerId");
            return (Criteria) this;
        }

        public Criteria andSubWorkerIdLessThanOrEqualTo(String value) {
            addCriterion("sub_worker_id <=", value, "subWorkerId");
            return (Criteria) this;
        }

        public Criteria andSubWorkerIdLike(String value) {
            addCriterion("sub_worker_id like", value, "subWorkerId");
            return (Criteria) this;
        }

        public Criteria andSubWorkerIdNotLike(String value) {
            addCriterion("sub_worker_id not like", value, "subWorkerId");
            return (Criteria) this;
        }

        public Criteria andSubWorkerIdIn(List<String> values) {
            addCriterion("sub_worker_id in", values, "subWorkerId");
            return (Criteria) this;
        }

        public Criteria andSubWorkerIdNotIn(List<String> values) {
            addCriterion("sub_worker_id not in", values, "subWorkerId");
            return (Criteria) this;
        }

        public Criteria andSubWorkerIdBetween(String value1, String value2) {
            addCriterion("sub_worker_id between", value1, value2, "subWorkerId");
            return (Criteria) this;
        }

        public Criteria andSubWorkerIdNotBetween(String value1, String value2) {
            addCriterion("sub_worker_id not between", value1, value2, "subWorkerId");
            return (Criteria) this;
        }

        public Criteria andLikeStateIsNull() {
            addCriterion("like_state is null");
            return (Criteria) this;
        }

        public Criteria andLikeStateIsNotNull() {
            addCriterion("like_state is not null");
            return (Criteria) this;
        }

        public Criteria andLikeStateEqualTo(Integer value) {
            addCriterion("like_state =", value, "likeState");
            return (Criteria) this;
        }

        public Criteria andLikeStateNotEqualTo(Integer value) {
            addCriterion("like_state <>", value, "likeState");
            return (Criteria) this;
        }

        public Criteria andLikeStateGreaterThan(Integer value) {
            addCriterion("like_state >", value, "likeState");
            return (Criteria) this;
        }

        public Criteria andLikeStateGreaterThanOrEqualTo(Integer value) {
            addCriterion("like_state >=", value, "likeState");
            return (Criteria) this;
        }

        public Criteria andLikeStateLessThan(Integer value) {
            addCriterion("like_state <", value, "likeState");
            return (Criteria) this;
        }

        public Criteria andLikeStateLessThanOrEqualTo(Integer value) {
            addCriterion("like_state <=", value, "likeState");
            return (Criteria) this;
        }

        public Criteria andLikeStateIn(List<Integer> values) {
            addCriterion("like_state in", values, "likeState");
            return (Criteria) this;
        }

        public Criteria andLikeStateNotIn(List<Integer> values) {
            addCriterion("like_state not in", values, "likeState");
            return (Criteria) this;
        }

        public Criteria andLikeStateBetween(Integer value1, Integer value2) {
            addCriterion("like_state between", value1, value2, "likeState");
            return (Criteria) this;
        }

        public Criteria andLikeStateNotBetween(Integer value1, Integer value2) {
            addCriterion("like_state not between", value1, value2, "likeState");
            return (Criteria) this;
        }

        public Criteria andLikeDateIsNull() {
            addCriterion("like_date is null");
            return (Criteria) this;
        }

        public Criteria andLikeDateIsNotNull() {
            addCriterion("like_date is not null");
            return (Criteria) this;
        }

        public Criteria andLikeDateEqualTo(Date value) {
            addCriterion("like_date =", value, "likeDate");
            return (Criteria) this;
        }

        public Criteria andLikeDateNotEqualTo(Date value) {
            addCriterion("like_date <>", value, "likeDate");
            return (Criteria) this;
        }

        public Criteria andLikeDateGreaterThan(Date value) {
            addCriterion("like_date >", value, "likeDate");
            return (Criteria) this;
        }

        public Criteria andLikeDateGreaterThanOrEqualTo(Date value) {
            addCriterion("like_date >=", value, "likeDate");
            return (Criteria) this;
        }

        public Criteria andLikeDateLessThan(Date value) {
            addCriterion("like_date <", value, "likeDate");
            return (Criteria) this;
        }

        public Criteria andLikeDateLessThanOrEqualTo(Date value) {
            addCriterion("like_date <=", value, "likeDate");
            return (Criteria) this;
        }

        public Criteria andLikeDateIn(List<Date> values) {
            addCriterion("like_date in", values, "likeDate");
            return (Criteria) this;
        }

        public Criteria andLikeDateNotIn(List<Date> values) {
            addCriterion("like_date not in", values, "likeDate");
            return (Criteria) this;
        }

        public Criteria andLikeDateBetween(Date value1, Date value2) {
            addCriterion("like_date between", value1, value2, "likeDate");
            return (Criteria) this;
        }

        public Criteria andLikeDateNotBetween(Date value1, Date value2) {
            addCriterion("like_date not between", value1, value2, "likeDate");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}