package com.trip.triplove.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class TripLoveWebMvcConfigurer implements WebMvcConfigurer {

    private final static String FILE_UPLOAD_DIC = "D:\\ctrip\\userImage\\upload\\";//上传文件的默认url前缀，根据部署设置自行修改

    //对于/upload/**等路径都将它映射到"file:" + Constants.FILE_UPLOAD_DIC（项目外的目录）
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/upload/**").addResourceLocations("file:" + FILE_UPLOAD_DIC);
        registry.addResourceHandler("/user-img/**").addResourceLocations("file:" + FILE_UPLOAD_DIC);
//        registry.addResourceHandler("/upload/**").addResourceLocations(Constants.FILE_UPLOAD_DIC);
//        registry.addResourceHandler("/goods-img/**").addResourceLocations(Constants.FILE_UPLOAD_DIC);
    }
}
