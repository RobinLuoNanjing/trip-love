package com.trip.triplove.service;

import com.trip.triplove.error.BusinessException;
import com.trip.triplove.mbg.model.UserInfo;
import com.trip.triplove.service.model.User;

import java.util.List;

public interface UserInfoService {

    //获取用户信息
    UserInfo getUserInfo(String workerId) throws BusinessException;

    //获取首页用户列表
    List<User> getUserList(String workerId, Integer minAge, Integer maxAge, List<String> cities, Integer pageNum, Integer pageSize, String sex);

    //修改用户信息
    void editUserInfo(UserInfo userInfo);
    //用户注册
    void register(UserInfo userInfo) throws BusinessException;

}
