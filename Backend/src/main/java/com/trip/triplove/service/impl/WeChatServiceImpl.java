package com.trip.triplove.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.kevinsawicki.http.HttpRequest;
import com.trip.triplove.service.WeChatService;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class WeChatServiceImpl implements WeChatService {
    @Override
    public String codetoopenid(String code) {
        Map<String, String> data = new HashMap<String, String>();
        data.put("appid", "wx5824dd2aa2012fba");
        data.put("secret", "71280081b40996c126e5d7d46c01e0c8");
        data.put("js_code", code);
        data.put("grant_type", "authorization_code");


        String response = HttpRequest.get("https://api.weixin.qq.com/sns/jscode2session").form(data).body();
        System.out.println("Response was: " + response);
        JSONObject obj= JSON.parseObject(response);//将json字符串转换为json对
        String openid = (String)obj.get("openid");
        System.out.println(obj);

        return openid;
    }
}
