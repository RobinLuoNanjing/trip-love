package com.trip.triplove.service;

import com.trip.triplove.mbg.model.UserInfo;

import java.util.List;

public interface MatchingService {

    //配对对象
    List<UserInfo> getUserSoulMate(String workerId);

    //谁喜欢我
    List<UserInfo> whoLikesMe(String workerId);

    int loveClick(String mainWorkerId, String subWorkerId, boolean isLike);


}
