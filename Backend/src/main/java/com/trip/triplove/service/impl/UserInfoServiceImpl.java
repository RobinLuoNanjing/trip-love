package com.trip.triplove.service.impl;

import com.github.pagehelper.PageHelper;
import com.trip.triplove.error.BusinessException;
import com.trip.triplove.error.EmBusinessError;
import com.trip.triplove.mbg.mapper.MatchingRecordMapper;
import com.trip.triplove.mbg.mapper.UserInfoMapper;
import com.trip.triplove.mbg.model.MatchingRecord;
import com.trip.triplove.mbg.model.UserInfo;
import com.trip.triplove.service.UserInfoService;
import com.trip.triplove.service.model.User;
import com.trip.triplove.validator.ValidationImpl;
import com.trip.triplove.validator.ValidationResult;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserInfoServiceImpl implements UserInfoService {

    @Autowired
    private UserInfoMapper userInfoMapper;

    @Autowired
    private MatchingRecordMapper matchingRecordMapper;

    @Autowired
    private ValidationImpl validation;

    @Override
    public UserInfo getUserInfo(String workerId) throws BusinessException {
        UserInfo userInfo = userInfoMapper.selectByPrimaryKey(workerId);
        if(userInfo == null){
            throw new BusinessException(EmBusinessError.USER_NOT_EXIST);
        }
        return userInfo;
    }

    @Override
    public List<User> getUserList(String workerId, Integer minAge, Integer maxAge, List<String> cities, Integer pageNum, Integer pageSize, String sex) {
        PageHelper.startPage(pageNum, pageSize);
        List<UserInfo> filterList = userInfoMapper.selectByAgeAndProvinces(workerId, minAge, maxAge, cities, sex);
        List<User> userListWithLike = new ArrayList<>();
        for(UserInfo  userInfo: filterList){
            User user = new User();
            BeanUtils.copyProperties(userInfo, user);
            MatchingRecord record = matchingRecordMapper.selectByMainAndSub(workerId, userInfo.getWorkerId());
            if(record != null && (record.getLikeState() == 1 || record.getLikeState() == 3))
                user.setIslike(true);
            userListWithLike.add(user);
        }
        return userListWithLike;
    }


    @Override
    public void editUserInfo(UserInfo userInfo) {
        if(userInfo == null || StringUtils.isEmpty(userInfo.getWorkerId())){
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
        }
        UserInfo user = userInfoMapper.selectByPrimaryKey(userInfo.getWorkerId());
        if(user == null){
            throw new BusinessException(EmBusinessError.USER_NOT_EXIST);
        }
        userInfoMapper.updateByPrimaryKeySelective(userInfo);
    }

    @Override
    public void register(UserInfo userInfo) throws BusinessException {
        if(userInfo == null){
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
        }

        ValidationResult result = validation.validate(userInfo);
        if(result.isHasError()){
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR, result.getErrMsg());
        }

        userInfo.setSoulmate("-1");
        userInfoMapper.insert(userInfo);

    }
}
