package com.trip.triplove.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.trip.triplove.mbg.mapper.MatchingRecordMapper;
import com.trip.triplove.mbg.mapper.UserInfoMapper;
import com.trip.triplove.mbg.model.MatchingRecord;
import com.trip.triplove.mbg.model.MatchingRecordExample;
import com.trip.triplove.mbg.model.UserInfo;
import com.trip.triplove.service.MatchingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.Arrays;
import java.util.List;

@Service
public class MatchingServiceImpl implements MatchingService {

    @Autowired
    private MatchingRecordMapper matchingRecordMapper;
    @Autowired
    private UserInfoMapper userInfoMapper;

    @Override
    public List<UserInfo> getUserSoulMate(String workerId) {
        List<UserInfo> userInfos = matchingRecordMapper.selectSoulMateByWorkerid(workerId);
        return userInfos;
    }

    @Override
    public List<UserInfo> whoLikesMe(String workerId) {
        return matchingRecordMapper.selectLikesmeByWokerid(workerId);
    }

    @Override
    public int loveClick(String mainWorkerId, String subWorkerId, boolean isLike) {
        //查询 A -> B 和 B -> A
         System.out.println(mainWorkerId + "/" +  subWorkerId + "/" + isLike);
         MatchingRecord matchingRecordA =  matchingRecordMapper.selectByMainAndSub(mainWorkerId, subWorkerId);
         MatchingRecord matchingRecordB =  matchingRecordMapper.selectByMainAndSub(subWorkerId, mainWorkerId);

         // 判断是喜欢还是取消喜欢
         if(isLike){
             return turnOnClickButton(matchingRecordA, matchingRecordB, mainWorkerId, subWorkerId);
         }else if(!isLike){  // 如果 isLike == false, 表示取消喜欢
             return turnOffClickButton(matchingRecordA, matchingRecordB, mainWorkerId, subWorkerId);
         }

         return -1;     // 返回 -1 表示取消喜欢
    }

    // 点击喜欢
    public int turnOnClickButton(MatchingRecord matchingRecordA, MatchingRecord matchingRecordB, String mainWorkerId, String subWorkerId){
        // 1. A -> B
        if(matchingRecordA == null){   // 如果A -> B 不在数据库中

            // 2. 生成 A -> B的数据
            matchingRecordA = new MatchingRecord();
            matchingRecordA.setMainWorkerId(mainWorkerId);
            matchingRecordA.setSubWorkerId(subWorkerId);
            matchingRecordA.setLikeState(1);
            matchingRecordA.setLikeDate(new Date(System.currentTimeMillis()));

            // 3. 插入数据
            if(matchingRecordB == null){   // 如果 B -> A 不存在，则直接插入 A 喜欢 B，状态为1
                matchingRecordMapper.insert(matchingRecordA);
            }else{
                // 如果 B 喜欢 A
                if(matchingRecordB.getLikeState() == 1){
                    //那么设置 A 和 B为相互喜欢，状态为3
                    matchingRecordA.setLikeState(3);
                    matchingRecordMapper.insert(matchingRecordA);

                    matchingRecordMapper.updateLikeState(matchingRecordA.getSubWorkerId(), matchingRecordA.getMainWorkerId(), 3);
                    return 1;
                }else{ // B -> A 的状态为 0(正常), 则插入 A 喜欢 B，状态为1
                    matchingRecordMapper.insert(matchingRecordA);
                }
            }
        }
        else{   // 如果 A -> B 原来存在于数据库中
            if(matchingRecordB == null){// 如果 B -> A 不存在
                matchingRecordMapper.updateLikeState(matchingRecordA.getMainWorkerId(), matchingRecordA.getSubWorkerId(), 1);
            }
            else{   // 如果 B -> A 存在
                // 如果 B 喜欢 A
                if(matchingRecordB.getLikeState() == 1){
                    //那么设置 A 和 B为相互喜欢，状态为3
                    matchingRecordMapper.updateLikeState(matchingRecordA.getMainWorkerId(), matchingRecordA.getSubWorkerId(), 3);
                    matchingRecordMapper.updateLikeState(matchingRecordB.getMainWorkerId(), matchingRecordB.getSubWorkerId(), 3);
                    return 1;
                }else if(matchingRecordB.getLikeState() == 0){ // B -> A 的状态为 0(正常), 则插入 A 喜欢 B，状态为1
                    matchingRecordMapper.updateLikeState(matchingRecordA.getMainWorkerId(), matchingRecordA.getSubWorkerId(), 1);
                }else if(matchingRecordB.getLikeState() == 3){
                    matchingRecordMapper.updateLikeState(matchingRecordA.getMainWorkerId(), matchingRecordA.getSubWorkerId(), 3);
                }
            }
        }

        return -1;
    }

    // 取消喜欢
    public int turnOffClickButton(MatchingRecord matchingRecordA, MatchingRecord matchingRecordB, String mainWorkerId, String subWorkerId){
        if(matchingRecordA == null){
            // 2. 生成 A -> B的数据
            matchingRecordA = new MatchingRecord();
            matchingRecordA.setMainWorkerId(mainWorkerId);
            matchingRecordA.setSubWorkerId(subWorkerId);
            matchingRecordA.setLikeState(0);
            matchingRecordA.setLikeDate(new Date(System.currentTimeMillis()));
            matchingRecordMapper.insert(matchingRecordA);
        }
        else{
            // 如果B -> A的数据不存在
            if(matchingRecordB == null){
                matchingRecordMapper.updateLikeState(matchingRecordA.getMainWorkerId(), matchingRecordA.getSubWorkerId(), 0);
            }else{
                // 如果B 和 A原来配对，则 B -> A 改为1
                if(matchingRecordB.getLikeState() == 3){
                    matchingRecordMapper.updateLikeState(matchingRecordB.getMainWorkerId(), matchingRecordB.getSubWorkerId(), 1);
                }
            }
            matchingRecordMapper.updateLikeState(matchingRecordA.getMainWorkerId(), matchingRecordA.getSubWorkerId(), 0);
        }

        return -1;
    }
}
