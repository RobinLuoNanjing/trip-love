package com.trip.triplove.service.model;

import com.trip.triplove.mbg.model.UserInfo;

public class User extends UserInfo {

    //正在浏览该user的用户是否点亮了红心
    boolean islike;

    public boolean isIslike() {
        return islike;
    }

    public void setIslike(boolean islike) {
        this.islike = islike;
    }
}
