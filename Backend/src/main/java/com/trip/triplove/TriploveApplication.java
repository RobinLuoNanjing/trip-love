package com.trip.triplove;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@SpringBootApplication
@MapperScan({"com.trip.triplove.mbg.mapper","com.trip.triplove.dao"})
public class TriploveApplication {

    public static void main(String[] args) {
        SpringApplication.run(TriploveApplication.class, args);
    }

}
